<?php
/**
 * @file
 * syclops_users.features.user_role.inc
 */

/**
 * Implements hook_user_default_roles().
 */
function syclops_users_user_default_roles() {
  $roles = array();

  // Exported role: project creator.
  $roles['project creator'] = array(
    'name' => 'project creator',
    'weight' => '2',
  );

  // Exported role: site admin.
  $roles['site admin'] = array(
    'name' => 'site admin',
    'weight' => '4',
  );

  return $roles;
}
