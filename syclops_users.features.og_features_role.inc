<?php
/**
 * @file
 * syclops_users.features.og_features_role.inc
 */

/**
 * Implements hook_og_features_default_roles().
 */
function syclops_users_og_features_default_roles() {
  $roles = array();

  // Exported OG Role: 'node:project:project admin'.
  $roles['node:project:project admin'] = array(
    'gid' => 0,
    'group_type' => 'node',
    'group_bundle' => 'project',
    'name' => 'project admin',
  );

  return $roles;
}
