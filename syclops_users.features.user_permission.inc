<?php
/**
 * @file
 * syclops_users.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function syclops_users_user_default_permissions() {
  $permissions = array();

  // Exported permission: access administration menu.
  $permissions['access administration menu'] = array(
    'name' => 'access administration menu',
    'roles' => array(),
    'module' => 'admin_menu',
  );

  // Exported permission: access administration pages.
  $permissions['access administration pages'] = array(
    'name' => 'access administration pages',
    'roles' => array(
      'site admin' => 'site admin',
    ),
    'module' => 'system',
  );

  // Exported permission: access all views.
  $permissions['access all views'] = array(
    'name' => 'access all views',
    'roles' => array(),
    'module' => 'views',
  );

  // Exported permission: access comments.
  $permissions['access comments'] = array(
    'name' => 'access comments',
    'roles' => array(
      'authenticated user' => 'authenticated user',
      'project creator' => 'project creator',
      'site admin' => 'site admin',
    ),
    'module' => 'comment',
  );

  // Exported permission: access content.
  $permissions['access content'] = array(
    'name' => 'access content',
    'roles' => array(
      'authenticated user' => 'authenticated user',
      'project creator' => 'project creator',
      'site admin' => 'site admin',
    ),
    'module' => 'node',
  );

  // Exported permission: access content overview.
  $permissions['access content overview'] = array(
    'name' => 'access content overview',
    'roles' => array(
      'site admin' => 'site admin',
    ),
    'module' => 'node',
  );

  // Exported permission: access rules debug.
  $permissions['access rules debug'] = array(
    'name' => 'access rules debug',
    'roles' => array(),
    'module' => 'rules',
  );

  // Exported permission: access site in maintenance mode.
  $permissions['access site in maintenance mode'] = array(
    'name' => 'access site in maintenance mode',
    'roles' => array(),
    'module' => 'system',
  );

  // Exported permission: access site reports.
  $permissions['access site reports'] = array(
    'name' => 'access site reports',
    'roles' => array(),
    'module' => 'system',
  );

  // Exported permission: access user profiles.
  $permissions['access user profiles'] = array(
    'name' => 'access user profiles',
    'roles' => array(
      'authenticated user' => 'authenticated user',
      'project creator' => 'project creator',
      'site admin' => 'site admin',
    ),
    'module' => 'user',
  );

  // Exported permission: admin tfa settings.
  $permissions['admin tfa settings'] = array(
    'name' => 'admin tfa settings',
    'roles' => array(),
    'module' => 'tfa',
  );

  // Exported permission: administer actions.
  $permissions['administer actions'] = array(
    'name' => 'administer actions',
    'roles' => array(),
    'module' => 'system',
  );

  // Exported permission: administer blocks.
  $permissions['administer blocks'] = array(
    'name' => 'administer blocks',
    'roles' => array(),
    'module' => 'block',
  );

  // Exported permission: administer comments.
  $permissions['administer comments'] = array(
    'name' => 'administer comments',
    'roles' => array(),
    'module' => 'comment',
  );

  // Exported permission: administer content types.
  $permissions['administer content types'] = array(
    'name' => 'administer content types',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: administer contexts.
  $permissions['administer contexts'] = array(
    'name' => 'administer contexts',
    'roles' => array(),
    'module' => 'context_ui',
  );

  // Exported permission: administer features.
  $permissions['administer features'] = array(
    'name' => 'administer features',
    'roles' => array(),
    'module' => 'features',
  );

  // Exported permission: administer fieldgroups.
  $permissions['administer fieldgroups'] = array(
    'name' => 'administer fieldgroups',
    'roles' => array(),
    'module' => 'field_group',
  );

  // Exported permission: administer filters.
  $permissions['administer filters'] = array(
    'name' => 'administer filters',
    'roles' => array(),
    'module' => 'filter',
  );

  // Exported permission: administer flags.
  $permissions['administer flags'] = array(
    'name' => 'administer flags',
    'roles' => array(),
    'module' => 'flag',
  );

  // Exported permission: administer group.
  $permissions['administer group'] = array(
    'name' => 'administer group',
    'roles' => array(),
    'module' => 'og',
  );

  // Exported permission: administer image styles.
  $permissions['administer image styles'] = array(
    'name' => 'administer image styles',
    'roles' => array(),
    'module' => 'image',
  );

  // Exported permission: administer menu.
  $permissions['administer menu'] = array(
    'name' => 'administer menu',
    'roles' => array(),
    'module' => 'menu',
  );

  // Exported permission: administer message types.
  $permissions['administer message types'] = array(
    'name' => 'administer message types',
    'roles' => array(),
    'module' => 'message',
  );

  // Exported permission: administer modules.
  $permissions['administer modules'] = array(
    'name' => 'administer modules',
    'roles' => array(),
    'module' => 'system',
  );

  // Exported permission: administer nodes.
  $permissions['administer nodes'] = array(
    'name' => 'administer nodes',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: administer pathauto.
  $permissions['administer pathauto'] = array(
    'name' => 'administer pathauto',
    'roles' => array(),
    'module' => 'pathauto',
  );

  // Exported permission: administer permissions.
  $permissions['administer permissions'] = array(
    'name' => 'administer permissions',
    'roles' => array(),
    'module' => 'user',
  );

  // Exported permission: administer realname.
  $permissions['administer realname'] = array(
    'name' => 'administer realname',
    'roles' => array(),
    'module' => 'realname',
  );

  // Exported permission: administer rules.
  $permissions['administer rules'] = array(
    'name' => 'administer rules',
    'roles' => array(
      'site admin' => 'site admin',
    ),
    'module' => 'rules',
  );

  // Exported permission: administer search.
  $permissions['administer search'] = array(
    'name' => 'administer search',
    'roles' => array(),
    'module' => 'search',
  );

  // Exported permission: administer session limits by role.
  $permissions['administer session limits by role'] = array(
    'name' => 'administer session limits by role',
    'roles' => array(),
    'module' => 'session_limit',
  );

  // Exported permission: administer session limits per user.
  $permissions['administer session limits per user'] = array(
    'name' => 'administer session limits per user',
    'roles' => array(),
    'module' => 'session_limit',
  );

  // Exported permission: administer site configuration.
  $permissions['administer site configuration'] = array(
    'name' => 'administer site configuration',
    'roles' => array(),
    'module' => 'system',
  );

  // Exported permission: administer smtp module.
  $permissions['administer smtp module'] = array(
    'name' => 'administer smtp module',
    'roles' => array(
      'site admin' => 'site admin',
    ),
    'module' => 'smtp',
  );

  // Exported permission: administer software updates.
  $permissions['administer software updates'] = array(
    'name' => 'administer software updates',
    'roles' => array(),
    'module' => 'system',
  );

  // Exported permission: administer themes.
  $permissions['administer themes'] = array(
    'name' => 'administer themes',
    'roles' => array(),
    'module' => 'system',
  );

  // Exported permission: administer url aliases.
  $permissions['administer url aliases'] = array(
    'name' => 'administer url aliases',
    'roles' => array(),
    'module' => 'path',
  );

  // Exported permission: administer users.
  $permissions['administer users'] = array(
    'name' => 'administer users',
    'roles' => array(
      'site admin' => 'site admin',
    ),
    'module' => 'user',
  );

  // Exported permission: administer views.
  $permissions['administer views'] = array(
    'name' => 'administer views',
    'roles' => array(),
    'module' => 'views',
  );

  // Exported permission: assign all roles.
  $permissions['assign all roles'] = array(
    'name' => 'assign all roles',
    'roles' => array(
      'site admin' => 'site admin',
    ),
    'module' => 'role_delegation',
  );

  // Exported permission: assign project creator role.
  $permissions['assign project creator role'] = array(
    'name' => 'assign project creator role',
    'roles' => array(),
    'module' => 'role_delegation',
  );

  // Exported permission: assign site admin role.
  $permissions['assign site admin role'] = array(
    'name' => 'assign site admin role',
    'roles' => array(),
    'module' => 'role_delegation',
  );

  // Exported permission: block IP addresses.
  $permissions['block IP addresses'] = array(
    'name' => 'block IP addresses',
    'roles' => array(),
    'module' => 'system',
  );

  // Exported permission: bypass node access.
  $permissions['bypass node access'] = array(
    'name' => 'bypass node access',
    'roles' => array(
      'site admin' => 'site admin',
    ),
    'module' => 'node',
  );

  // Exported permission: bypass rules access.
  $permissions['bypass rules access'] = array(
    'name' => 'bypass rules access',
    'roles' => array(
      'site admin' => 'site admin',
    ),
    'module' => 'rules',
  );

  // Exported permission: cancel account.
  $permissions['cancel account'] = array(
    'name' => 'cancel account',
    'roles' => array(),
    'module' => 'user',
  );

  // Exported permission: change own username.
  $permissions['change own username'] = array(
    'name' => 'change own username',
    'roles' => array(
      'site admin' => 'site admin',
    ),
    'module' => 'user',
  );

  // Exported permission: create grant content.
  $permissions['create grant content'] = array(
    'name' => 'create grant content',
    'roles' => array(
      'site admin' => 'site admin',
    ),
    'module' => 'node',
  );

  // Exported permission: create messages.
  $permissions['create messages'] = array(
    'name' => 'create messages',
    'roles' => array(
      'authenticated user' => 'authenticated user',
      'project creator' => 'project creator',
      'site admin' => 'site admin',
    ),
    'module' => 'message',
  );

  // Exported permission: create project content.
  $permissions['create project content'] = array(
    'name' => 'create project content',
    'roles' => array(
      'project creator' => 'project creator',
      'site admin' => 'site admin',
    ),
    'module' => 'node',
  );

  // Exported permission: create session content.
  $permissions['create session content'] = array(
    'name' => 'create session content',
    'roles' => array(
      'site admin' => 'site admin',
    ),
    'module' => 'node',
  );

  // Exported permission: create ssh_config content.
  $permissions['create ssh_config content'] = array(
    'name' => 'create ssh_config content',
    'roles' => array(
      'site admin' => 'site admin',
    ),
    'module' => 'node',
  );

  // Exported permission: create url aliases.
  $permissions['create url aliases'] = array(
    'name' => 'create url aliases',
    'roles' => array(),
    'module' => 'path',
  );

  // Exported permission: delete any grant content.
  $permissions['delete any grant content'] = array(
    'name' => 'delete any grant content',
    'roles' => array(
      'site admin' => 'site admin',
    ),
    'module' => 'node',
  );

  // Exported permission: delete any project content.
  $permissions['delete any project content'] = array(
    'name' => 'delete any project content',
    'roles' => array(
      'site admin' => 'site admin',
    ),
    'module' => 'node',
  );

  // Exported permission: delete any session content.
  $permissions['delete any session content'] = array(
    'name' => 'delete any session content',
    'roles' => array(
      'site admin' => 'site admin',
    ),
    'module' => 'node',
  );

  // Exported permission: delete any ssh_config content.
  $permissions['delete any ssh_config content'] = array(
    'name' => 'delete any ssh_config content',
    'roles' => array(
      'site admin' => 'site admin',
    ),
    'module' => 'node',
  );

  // Exported permission: delete own grant content.
  $permissions['delete own grant content'] = array(
    'name' => 'delete own grant content',
    'roles' => array(
      'site admin' => 'site admin',
    ),
    'module' => 'node',
  );

  // Exported permission: delete own project content.
  $permissions['delete own project content'] = array(
    'name' => 'delete own project content',
    'roles' => array(
      'site admin' => 'site admin',
    ),
    'module' => 'node',
  );

  // Exported permission: delete own session content.
  $permissions['delete own session content'] = array(
    'name' => 'delete own session content',
    'roles' => array(
      'site admin' => 'site admin',
    ),
    'module' => 'node',
  );

  // Exported permission: delete own ssh_config content.
  $permissions['delete own ssh_config content'] = array(
    'name' => 'delete own ssh_config content',
    'roles' => array(
      'site admin' => 'site admin',
    ),
    'module' => 'node',
  );

  // Exported permission: delete revisions.
  $permissions['delete revisions'] = array(
    'name' => 'delete revisions',
    'roles' => array(
      'site admin' => 'site admin',
    ),
    'module' => 'node',
  );

  // Exported permission: display drupal links.
  $permissions['display drupal links'] = array(
    'name' => 'display drupal links',
    'roles' => array(),
    'module' => 'admin_menu',
  );

  // Exported permission: edit any grant content.
  $permissions['edit any grant content'] = array(
    'name' => 'edit any grant content',
    'roles' => array(
      'site admin' => 'site admin',
    ),
    'module' => 'node',
  );

  // Exported permission: edit any project content.
  $permissions['edit any project content'] = array(
    'name' => 'edit any project content',
    'roles' => array(
      'site admin' => 'site admin',
    ),
    'module' => 'node',
  );

  // Exported permission: edit any session content.
  $permissions['edit any session content'] = array(
    'name' => 'edit any session content',
    'roles' => array(
      'site admin' => 'site admin',
    ),
    'module' => 'node',
  );

  // Exported permission: edit any ssh_config content.
  $permissions['edit any ssh_config content'] = array(
    'name' => 'edit any ssh_config content',
    'roles' => array(
      'site admin' => 'site admin',
    ),
    'module' => 'node',
  );

  // Exported permission: edit own comments.
  $permissions['edit own comments'] = array(
    'name' => 'edit own comments',
    'roles' => array(
      'authenticated user' => 'authenticated user',
      'project creator' => 'project creator',
      'site admin' => 'site admin',
    ),
    'module' => 'comment',
  );

  // Exported permission: edit own grant content.
  $permissions['edit own grant content'] = array(
    'name' => 'edit own grant content',
    'roles' => array(
      'site admin' => 'site admin',
    ),
    'module' => 'node',
  );

  // Exported permission: edit own project content.
  $permissions['edit own project content'] = array(
    'name' => 'edit own project content',
    'roles' => array(
      'project creator' => 'project creator',
      'site admin' => 'site admin',
    ),
    'module' => 'node',
  );

  // Exported permission: edit own session content.
  $permissions['edit own session content'] = array(
    'name' => 'edit own session content',
    'roles' => array(
      'site admin' => 'site admin',
    ),
    'module' => 'node',
  );

  // Exported permission: edit own ssh_config content.
  $permissions['edit own ssh_config content'] = array(
    'name' => 'edit own ssh_config content',
    'roles' => array(
      'site admin' => 'site admin',
    ),
    'module' => 'node',
  );

  // Exported permission: flag local_archive.
  $permissions['flag local_archive'] = array(
    'name' => 'flag local_archive',
    'roles' => array(
      'authenticated user' => 'authenticated user',
      'project creator' => 'project creator',
      'site admin' => 'site admin',
    ),
    'module' => 'flag',
  );

  // Exported permission: flag message_archive.
  $permissions['flag message_archive'] = array(
    'name' => 'flag message_archive',
    'roles' => array(
      'authenticated user' => 'authenticated user',
      'project creator' => 'project creator',
      'site admin' => 'site admin',
    ),
    'module' => 'flag',
  );

  // Exported permission: flag parent_archive.
  $permissions['flag parent_archive'] = array(
    'name' => 'flag parent_archive',
    'roles' => array(
      'authenticated user' => 'authenticated user',
      'project creator' => 'project creator',
      'site admin' => 'site admin',
    ),
    'module' => 'flag',
  );

  // Exported permission: flush caches.
  $permissions['flush caches'] = array(
    'name' => 'flush caches',
    'roles' => array(),
    'module' => 'admin_menu',
  );

  // Exported permission: manage features.
  $permissions['manage features'] = array(
    'name' => 'manage features',
    'roles' => array(),
    'module' => 'features',
  );

  // Exported permission: notify of path changes.
  $permissions['notify of path changes'] = array(
    'name' => 'notify of path changes',
    'roles' => array(),
    'module' => 'pathauto',
  );

  // Exported permission: post comments.
  $permissions['post comments'] = array(
    'name' => 'post comments',
    'roles' => array(
      'authenticated user' => 'authenticated user',
      'project creator' => 'project creator',
      'site admin' => 'site admin',
    ),
    'module' => 'comment',
  );

  // Exported permission: require tfa.
  $permissions['require tfa'] = array(
    'name' => 'require tfa',
    'roles' => array(),
    'module' => 'tfa',
  );

  // Exported permission: revert revisions.
  $permissions['revert revisions'] = array(
    'name' => 'revert revisions',
    'roles' => array(
      'site admin' => 'site admin',
    ),
    'module' => 'node',
  );

  // Exported permission: search content.
  $permissions['search content'] = array(
    'name' => 'search content',
    'roles' => array(),
    'module' => 'search',
  );

  // Exported permission: select account cancellation method.
  $permissions['select account cancellation method'] = array(
    'name' => 'select account cancellation method',
    'roles' => array(),
    'module' => 'user',
  );

  // Exported permission: setup own tfa.
  $permissions['setup own tfa'] = array(
    'name' => 'setup own tfa',
    'roles' => array(
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'tfa_basic',
  );

  // Exported permission: skip comment approval.
  $permissions['skip comment approval'] = array(
    'name' => 'skip comment approval',
    'roles' => array(
      'authenticated user' => 'authenticated user',
      'project creator' => 'project creator',
      'site admin' => 'site admin',
    ),
    'module' => 'comment',
  );

  // Exported permission: skip tfa.
  $permissions['skip tfa'] = array(
    'name' => 'skip tfa',
    'roles' => array(),
    'module' => 'tfa',
  );

  // Exported permission: unflag local_archive.
  $permissions['unflag local_archive'] = array(
    'name' => 'unflag local_archive',
    'roles' => array(
      'authenticated user' => 'authenticated user',
      'project creator' => 'project creator',
      'site admin' => 'site admin',
    ),
    'module' => 'flag',
  );

  // Exported permission: unflag message_archive.
  $permissions['unflag message_archive'] = array(
    'name' => 'unflag message_archive',
    'roles' => array(
      'authenticated user' => 'authenticated user',
      'project creator' => 'project creator',
      'site admin' => 'site admin',
    ),
    'module' => 'flag',
  );

  // Exported permission: unflag parent_archive.
  $permissions['unflag parent_archive'] = array(
    'name' => 'unflag parent_archive',
    'roles' => array(
      'authenticated user' => 'authenticated user',
      'project creator' => 'project creator',
      'site admin' => 'site admin',
    ),
    'module' => 'flag',
  );

  // Exported permission: unique_field_perm_admin.
  $permissions['unique_field_perm_admin'] = array(
    'name' => 'unique_field_perm_admin',
    'roles' => array(),
    'module' => 'unique_field',
  );

  // Exported permission: unique_field_perm_bypass.
  $permissions['unique_field_perm_bypass'] = array(
    'name' => 'unique_field_perm_bypass',
    'roles' => array(),
    'module' => 'unique_field',
  );

  // Exported permission: use PHP for settings.
  $permissions['use PHP for settings'] = array(
    'name' => 'use PHP for settings',
    'roles' => array(),
    'module' => 'php',
  );

  // Exported permission: use PHP for title patterns.
  $permissions['use PHP for title patterns'] = array(
    'name' => 'use PHP for title patterns',
    'roles' => array(),
    'module' => 'auto_nodetitle',
  );

  // Exported permission: use advanced search.
  $permissions['use advanced search'] = array(
    'name' => 'use advanced search',
    'roles' => array(),
    'module' => 'search',
  );

  // Exported permission: use flag import.
  $permissions['use flag import'] = array(
    'name' => 'use flag import',
    'roles' => array(),
    'module' => 'flag',
  );

  // Exported permission: use text format full_html.
  $permissions['use text format full_html'] = array(
    'name' => 'use text format full_html',
    'roles' => array(),
    'module' => 'filter',
  );

  // Exported permission: use text format php_code.
  $permissions['use text format php_code'] = array(
    'name' => 'use text format php_code',
    'roles' => array(),
    'module' => 'filter',
  );

  // Exported permission: view own unpublished content.
  $permissions['view own unpublished content'] = array(
    'name' => 'view own unpublished content',
    'roles' => array(
      'site admin' => 'site admin',
    ),
    'module' => 'node',
  );

  // Exported permission: view revisions.
  $permissions['view revisions'] = array(
    'name' => 'view revisions',
    'roles' => array(
      'site admin' => 'site admin',
    ),
    'module' => 'node',
  );

  // Exported permission: view the administration theme.
  $permissions['view the administration theme'] = array(
    'name' => 'view the administration theme',
    'roles' => array(
      'site admin' => 'site admin',
    ),
    'module' => 'system',
  );

  return $permissions;
}
