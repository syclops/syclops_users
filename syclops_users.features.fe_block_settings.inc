<?php
/**
 * @file
 * syclops_users.features.fe_block_settings.inc
 */

/**
 * Implements hook_default_fe_block_settings().
 */
function syclops_users_default_fe_block_settings() {
  $export = array();

  $export['version'] = '2.0';

  $export['block-powered_by'] = array(
    'cache' => -1,
    'custom' => '0',
    'machine_name' => 'powered_by',
    'module' => 'block',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'syclops' => array(
        'region' => 'footer',
        'status' => '1',
        'theme' => 'syclops',
        'weight' => '0',
      ),
    ),
    'title' => '',
    'visibility' => '0',
  );

  $export['block-summary_block_user_page'] = array(
    'cache' => -1,
    'custom' => '0',
    'machine_name' => 'summary_block_user_page',
    'module' => 'block',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'syclops' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'syclops',
        'weight' => '0',
      ),
    ),
    'title' => '',
    'visibility' => '0',
  );

  $export['block-syclops_highlighted_menus'] = array(
    'cache' => -1,
    'custom' => '0',
    'machine_name' => 'syclops_highlighted_menus',
    'module' => 'block',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'syclops' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'syclops',
        'weight' => '0',
      ),
    ),
    'title' => '',
    'visibility' => '0',
  );

  $export['system-user-menu'] = array(
    'cache' => -1,
    'custom' => '0',
    'delta' => 'user-menu',
    'module' => 'system',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'syclops' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'syclops',
        'weight' => '0',
      ),
    ),
    'title' => '',
    'visibility' => '0',
  );

  $export['user-login'] = array(
    'cache' => -1,
    'custom' => '0',
    'delta' => 'login',
    'module' => 'user',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(
      'anonymous user' => '1',
    ),
    'themes' => array(
      'syclops' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'syclops',
        'weight' => '0',
      ),
    ),
    'title' => '',
    'visibility' => '1',
  );

  $export['views-syclops_project-block_5'] = array(
    'cache' => -1,
    'custom' => '0',
    'delta' => 'syclops_project-block_5',
    'module' => 'views',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'syclops' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'syclops',
        'weight' => '0',
      ),
    ),
    'title' => '',
    'visibility' => '0',
  );

  $export['views-syclops_summary-block_8'] = array(
    'cache' => -1,
    'custom' => '0',
    'delta' => 'syclops_summary-block_8',
    'module' => 'views',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'syclops' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'syclops',
        'weight' => '0',
      ),
    ),
    'title' => '',
    'visibility' => '0',
  );

  $export['views-syclops_summary-block_9'] = array(
    'cache' => -1,
    'custom' => '0',
    'delta' => 'syclops_summary-block_9',
    'module' => 'views',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'syclops' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'syclops',
        'weight' => '0',
      ),
    ),
    'title' => '',
    'visibility' => '0',
  );

  $export['views-syclops_user-block_1'] = array(
    'cache' => -1,
    'custom' => '0',
    'delta' => 'syclops_user-block_1',
    'module' => 'views',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'syclops' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'syclops',
        'weight' => '0',
      ),
    ),
    'title' => '',
    'visibility' => '0',
  );

  $export['views-syclops_user-block_2'] = array(
    'cache' => -1,
    'custom' => '0',
    'delta' => 'syclops_user-block_2',
    'module' => 'views',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'syclops' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'syclops',
        'weight' => '0',
      ),
    ),
    'title' => '',
    'visibility' => '0',
  );

  $export['views-syclops_user-block_3'] = array(
    'cache' => -1,
    'custom' => '0',
    'delta' => 'syclops_user-block_3',
    'module' => 'views',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'syclops' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'syclops',
        'weight' => '0',
      ),
    ),
    'title' => '',
    'visibility' => '0',
  );

  $export['views-syclops_user-block_4'] = array(
    'cache' => -1,
    'custom' => '0',
    'delta' => 'syclops_user-block_4',
    'module' => 'views',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'syclops' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'syclops',
        'weight' => '0',
      ),
    ),
    'title' => '',
    'visibility' => '0',
  );

  $export['views-syclops_user-block_5'] = array(
    'cache' => -1,
    'custom' => '0',
    'delta' => 'syclops_user-block_5',
    'module' => 'views',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'syclops' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'syclops',
        'weight' => '0',
      ),
    ),
    'title' => '',
    'visibility' => '0',
  );

  return $export;
}
