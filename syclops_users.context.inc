<?php
/**
 * @file
 * syclops_users.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function syclops_users_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'user-profile';
  $context->description = '';
  $context->tag = 'User';
  $context->conditions = array(
    'user_page' => array(
      'values' => array(
        'view' => 'view',
      ),
      'options' => array(
        'mode' => 'all',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'block-3' => array(
          'module' => 'block',
          'delta' => '3',
          'region' => 'highlighted',
          'weight' => '-10',
        ),
        'views-syclops_activities-block_8' => array(
          'module' => 'views',
          'delta' => 'syclops_activities-block_8',
          'region' => 'content',
          'weight' => '-10',
        ),
        'views-syclops_user-block_3' => array(
          'module' => 'views',
          'delta' => 'syclops_user-block_3',
          'region' => 'sidebar_second',
          'weight' => '-10',
        ),
        'block-2' => array(
          'module' => 'block',
          'delta' => '2',
          'region' => 'sidebar_second',
          'weight' => '-9',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('User');
  $export['user-profile'] = $context;

  return $export;
}
