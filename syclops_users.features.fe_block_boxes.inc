<?php
/**
 * @file
 * syclops_users.features.fe_block_boxes.inc
 */

/**
 * Implements hook_default_fe_block_boxes().
 */
function syclops_users_default_fe_block_boxes() {
  $export = array();

  $fe_block_boxes = new stdClass();
  $fe_block_boxes->info = 'Powered By';
  $fe_block_boxes->format = 'php_code';
  $fe_block_boxes->machine_name = 'powered_by';
  $fe_block_boxes->body = '<p class="text-right"><small><a href="http://www.syclops.io" target="_blank">Syclops</a> is registered trademark of TBW International Pte. Ltd. Copyright <?php print date(\'Y\', time());?> TBW International Pte. Ltd. All Rights Reserved.</small></p>';

  $export['powered_by'] = $fe_block_boxes;

  $fe_block_boxes = new stdClass();
  $fe_block_boxes->info = 'Page Summary Block(User)';
  $fe_block_boxes->format = 'php_code';
  $fe_block_boxes->machine_name = 'summary_block_user_page';
  $fe_block_boxes->body = '<?php 
 syclops_ui_user_page_summary_block_generator(arg(1));
?>';

  $export['summary_block_user_page'] = $fe_block_boxes;

  $fe_block_boxes = new stdClass();
  $fe_block_boxes->info = 'Syclops Highlighted Menus';
  $fe_block_boxes->format = 'php_code';
  $fe_block_boxes->machine_name = 'syclops_highlighted_menus';
  $fe_block_boxes->body = ' <?php
  syclops_ui_menu_generator();
?>';

  $export['syclops_highlighted_menus'] = $fe_block_boxes;

  return $export;
}
