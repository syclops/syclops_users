<?php
/**
 * @file
 * syclops_users.rules_defaults.inc
 */

/**
 * Implements hook_default_rules_configuration().
 */
function syclops_users_default_rules_configuration() {
  $items = array();
  $items['rules_user_logged_in'] = entity_import('rules_config', '{ "rules_user_logged_in" : {
      "LABEL" : "User Logged In",
      "PLUGIN" : "reaction rule",
      "TAGS" : [ "user" ],
      "REQUIRES" : [ "rules" ],
      "ON" : [ "user_login" ],
      "DO" : [ { "redirect" : { "url" : "dashboard" } } ]
    }
  }');
  $items['rules_user_logged_out'] = entity_import('rules_config', '{ "rules_user_logged_out" : {
      "LABEL" : "User Logged Out",
      "PLUGIN" : "reaction rule",
      "TAGS" : [ "user" ],
      "REQUIRES" : [ "rules" ],
      "ON" : [ "user_logout" ],
      "DO" : [ { "redirect" : { "url" : "user\\/login" } } ]
    }
  }');
  return $items;
}
