<?php
/**
 * @file
 * syclops_users.features.og_features_permission.inc
 */

/**
 * Implements hook_og_features_default_permissions().
 */
function syclops_users_og_features_default_permissions() {
  $permissions = array();

  // Exported og permission: 'node:project:add user'
  $permissions['node:project:add user'] = array(
    'roles' => array(
      'project admin' => 'project admin',
    ),
  );

  // Exported og permission: 'node:project:administer group'
  $permissions['node:project:administer group'] = array(
    'roles' => array(),
  );

  // Exported og permission: 'node:project:approve and deny subscription'
  $permissions['node:project:approve and deny subscription'] = array(
    'roles' => array(
      'project admin' => 'project admin',
    ),
  );

  // Exported og permission: 'node:project:create grant content'
  $permissions['node:project:create grant content'] = array(
    'roles' => array(
      'project admin' => 'project admin',
    ),
  );

  // Exported og permission: 'node:project:create session content'
  $permissions['node:project:create session content'] = array(
    'roles' => array(
      'member' => 'member',
      'project admin' => 'project admin',
    ),
  );

  // Exported og permission: 'node:project:create ssh_config content'
  $permissions['node:project:create ssh_config content'] = array(
    'roles' => array(
      'project admin' => 'project admin',
    ),
  );

  // Exported og permission: 'node:project:delete any grant content'
  $permissions['node:project:delete any grant content'] = array(
    'roles' => array(),
  );

  // Exported og permission: 'node:project:delete any session content'
  $permissions['node:project:delete any session content'] = array(
    'roles' => array(),
  );

  // Exported og permission: 'node:project:delete any ssh_config content'
  $permissions['node:project:delete any ssh_config content'] = array(
    'roles' => array(),
  );

  // Exported og permission: 'node:project:delete own grant content'
  $permissions['node:project:delete own grant content'] = array(
    'roles' => array(),
  );

  // Exported og permission: 'node:project:delete own session content'
  $permissions['node:project:delete own session content'] = array(
    'roles' => array(),
  );

  // Exported og permission: 'node:project:delete own ssh_config content'
  $permissions['node:project:delete own ssh_config content'] = array(
    'roles' => array(),
  );

  // Exported og permission: 'node:project:manage members'
  $permissions['node:project:manage members'] = array(
    'roles' => array(),
  );

  // Exported og permission: 'node:project:manage permissions'
  $permissions['node:project:manage permissions'] = array(
    'roles' => array(),
  );

  // Exported og permission: 'node:project:manage roles'
  $permissions['node:project:manage roles'] = array(
    'roles' => array(),
  );

  // Exported og permission: 'node:project:subscribe'
  $permissions['node:project:subscribe'] = array(
    'roles' => array(),
  );

  // Exported og permission: 'node:project:subscribe without approval'
  $permissions['node:project:subscribe without approval'] = array(
    'roles' => array(),
  );

  // Exported og permission: 'node:project:unsubscribe'
  $permissions['node:project:unsubscribe'] = array(
    'roles' => array(),
  );

  // Exported og permission: 'node:project:update any grant content'
  $permissions['node:project:update any grant content'] = array(
    'roles' => array(
      'project admin' => 'project admin',
    ),
  );

  // Exported og permission: 'node:project:update any session content'
  $permissions['node:project:update any session content'] = array(
    'roles' => array(
      'project admin' => 'project admin',
    ),
  );

  // Exported og permission: 'node:project:update any ssh_config content'
  $permissions['node:project:update any ssh_config content'] = array(
    'roles' => array(
      'project admin' => 'project admin',
    ),
  );

  // Exported og permission: 'node:project:update field_grant_ssh_config field'
  $permissions['node:project:update field_grant_ssh_config field'] = array(
    'roles' => array(
      'project admin' => 'project admin',
    ),
  );

  // Exported og permission: 'node:project:update field_grant_status field'
  $permissions['node:project:update field_grant_status field'] = array(
    'roles' => array(
      'project admin' => 'project admin',
    ),
  );

  // Exported og permission: 'node:project:update field_grant_user field'
  $permissions['node:project:update field_grant_user field'] = array(
    'roles' => array(
      'project admin' => 'project admin',
    ),
  );

  // Exported og permission: 'node:project:update field_session_grant field'
  $permissions['node:project:update field_session_grant field'] = array(
    'roles' => array(
      'project admin' => 'project admin',
    ),
  );

  // Exported og permission: 'node:project:update field_session_id field'
  $permissions['node:project:update field_session_id field'] = array(
    'roles' => array(
      'project admin' => 'project admin',
    ),
  );

  // Exported og permission: 'node:project:update field_session_log field'
  $permissions['node:project:update field_session_log field'] = array(
    'roles' => array(
      'project admin' => 'project admin',
    ),
  );

  // Exported og permission: 'node:project:update field_session_playback field'
  $permissions['node:project:update field_session_playback field'] = array(
    'roles' => array(
      'project admin' => 'project admin',
    ),
  );

  // Exported og permission: 'node:project:update field_session_render_playback field'
  $permissions['node:project:update field_session_render_playback field'] = array(
    'roles' => array(
      'project admin' => 'project admin',
    ),
  );

  // Exported og permission: 'node:project:update field_session_status field'
  $permissions['node:project:update field_session_status field'] = array(
    'roles' => array(
      'project admin' => 'project admin',
    ),
  );

  // Exported og permission: 'node:project:update field_ssh_config_machine field'
  $permissions['node:project:update field_ssh_config_machine field'] = array(
    'roles' => array(
      'project admin' => 'project admin',
    ),
  );

  // Exported og permission: 'node:project:update field_ssh_config_machine_user field'
  $permissions['node:project:update field_ssh_config_machine_user field'] = array(
    'roles' => array(
      'project admin' => 'project admin',
    ),
  );

  // Exported og permission: 'node:project:update field_ssh_config_private_key field'
  $permissions['node:project:update field_ssh_config_private_key field'] = array(
    'roles' => array(
      'project admin' => 'project admin',
    ),
  );

  // Exported og permission: 'node:project:update field_syclops_config_max_session field'
  $permissions['node:project:update field_syclops_config_max_session field'] = array(
    'roles' => array(
      'project admin' => 'project admin',
    ),
  );

  // Exported og permission: 'node:project:update field_user_fname field'
  $permissions['node:project:update field_user_fname field'] = array(
    'roles' => array(
      'project admin' => 'project admin',
    ),
  );

  // Exported og permission: 'node:project:update field_user_lname field'
  $permissions['node:project:update field_user_lname field'] = array(
    'roles' => array(
      'project admin' => 'project admin',
    ),
  );

  // Exported og permission: 'node:project:update group'
  $permissions['node:project:update group'] = array(
    'roles' => array(
      'project admin' => 'project admin',
    ),
  );

  // Exported og permission: 'node:project:update group_group field'
  $permissions['node:project:update group_group field'] = array(
    'roles' => array(
      'project admin' => 'project admin',
    ),
  );

  // Exported og permission: 'node:project:update og_group_ref field'
  $permissions['node:project:update og_group_ref field'] = array(
    'roles' => array(
      'project admin' => 'project admin',
    ),
  );

  // Exported og permission: 'node:project:update og_user_node field'
  $permissions['node:project:update og_user_node field'] = array(
    'roles' => array(
      'project admin' => 'project admin',
    ),
  );

  // Exported og permission: 'node:project:update own grant content'
  $permissions['node:project:update own grant content'] = array(
    'roles' => array(
      'project admin' => 'project admin',
    ),
  );

  // Exported og permission: 'node:project:update own session content'
  $permissions['node:project:update own session content'] = array(
    'roles' => array(
      'member' => 'member',
      'project admin' => 'project admin',
    ),
  );

  // Exported og permission: 'node:project:update own ssh_config content'
  $permissions['node:project:update own ssh_config content'] = array(
    'roles' => array(
      'project admin' => 'project admin',
    ),
  );

  // Exported og permission: 'node:project:view field_grant_ssh_config field'
  $permissions['node:project:view field_grant_ssh_config field'] = array(
    'roles' => array(
      'member' => 'member',
      'project admin' => 'project admin',
    ),
  );

  // Exported og permission: 'node:project:view field_grant_status field'
  $permissions['node:project:view field_grant_status field'] = array(
    'roles' => array(
      'member' => 'member',
      'project admin' => 'project admin',
    ),
  );

  // Exported og permission: 'node:project:view field_grant_user field'
  $permissions['node:project:view field_grant_user field'] = array(
    'roles' => array(
      'member' => 'member',
      'project admin' => 'project admin',
    ),
  );

  // Exported og permission: 'node:project:view field_session_grant field'
  $permissions['node:project:view field_session_grant field'] = array(
    'roles' => array(
      'member' => 'member',
      'project admin' => 'project admin',
    ),
  );

  // Exported og permission: 'node:project:view field_session_id field'
  $permissions['node:project:view field_session_id field'] = array(
    'roles' => array(
      'member' => 'member',
      'project admin' => 'project admin',
    ),
  );

  // Exported og permission: 'node:project:view field_session_log field'
  $permissions['node:project:view field_session_log field'] = array(
    'roles' => array(
      'member' => 'member',
      'project admin' => 'project admin',
    ),
  );

  // Exported og permission: 'node:project:view field_session_playback field'
  $permissions['node:project:view field_session_playback field'] = array(
    'roles' => array(
      'member' => 'member',
      'project admin' => 'project admin',
    ),
  );

  // Exported og permission: 'node:project:view field_session_render_playback field'
  $permissions['node:project:view field_session_render_playback field'] = array(
    'roles' => array(
      'member' => 'member',
      'project admin' => 'project admin',
    ),
  );

  // Exported og permission: 'node:project:view field_session_status field'
  $permissions['node:project:view field_session_status field'] = array(
    'roles' => array(
      'member' => 'member',
      'project admin' => 'project admin',
    ),
  );

  // Exported og permission: 'node:project:view field_ssh_config_machine field'
  $permissions['node:project:view field_ssh_config_machine field'] = array(
    'roles' => array(
      'member' => 'member',
      'project admin' => 'project admin',
    ),
  );

  // Exported og permission: 'node:project:view field_ssh_config_machine_user field'
  $permissions['node:project:view field_ssh_config_machine_user field'] = array(
    'roles' => array(
      'member' => 'member',
      'project admin' => 'project admin',
    ),
  );

  // Exported og permission: 'node:project:view field_ssh_config_private_key field'
  $permissions['node:project:view field_ssh_config_private_key field'] = array(
    'roles' => array(
      'project admin' => 'project admin',
    ),
  );

  // Exported og permission: 'node:project:view field_syclops_config_max_session field'
  $permissions['node:project:view field_syclops_config_max_session field'] = array(
    'roles' => array(
      'project admin' => 'project admin',
    ),
  );

  // Exported og permission: 'node:project:view field_user_fname field'
  $permissions['node:project:view field_user_fname field'] = array(
    'roles' => array(
      'project admin' => 'project admin',
    ),
  );

  // Exported og permission: 'node:project:view field_user_lname field'
  $permissions['node:project:view field_user_lname field'] = array(
    'roles' => array(
      'project admin' => 'project admin',
    ),
  );

  // Exported og permission: 'node:project:view group_group field'
  $permissions['node:project:view group_group field'] = array(
    'roles' => array(
      'member' => 'member',
      'project admin' => 'project admin',
    ),
  );

  // Exported og permission: 'node:project:view og_group_ref field'
  $permissions['node:project:view og_group_ref field'] = array(
    'roles' => array(
      'member' => 'member',
      'project admin' => 'project admin',
    ),
  );

  // Exported og permission: 'node:project:view og_user_node field'
  $permissions['node:project:view og_user_node field'] = array(
    'roles' => array(
      'member' => 'member',
      'project admin' => 'project admin',
    ),
  );

  return $permissions;
}
